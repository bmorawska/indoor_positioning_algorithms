import pytest
from src.UWB.geometry import *


def test_center_is_correct():
    """check if calculating of centre of gravity of triangle is made correctly"""
    vertices = np.array([[1, -2.5],
                         [2, -4],
                         [1, -4]])

    center = count_center(vertices)

    assert center[0] == pytest.approx(1.33, 0.01)  # x
    assert center[1] == pytest.approx(-3.53, 0.01)  # y


def test_count_triangle_vertex_separated_on_x():
    """
    check if vertex of triangle is calculated correctly when circles are separated and on x axis (the easiest case)
    see picture visualizing the situation (./pictures/test_count_triangle_vertex_separated_on_x.jpg)
    """
    c1 = [1, 0]
    c2 = [5, 0]

    r1 = 1
    r2 = 2

    vertex = count_triangle_vertex(c1, r1, c2, r2)
    assert vertex[0] == pytest.approx(2.33, 0.01)  # x
    assert vertex[1] == pytest.approx(0, 0.01)  # y


def test_count_triangle_vertex_intersected_on_x():
    """
    check if vertex of triangle is calculated correctly when circles intersects and on x axis
    see picture visualizing the situation (./pictures/test_count_triangle_vertex_intersected_on_x.jpg)
    """
    c1 = [1, 0]
    c2 = [6, 0]

    r1 = 2
    r2 = 4

    vertex = count_triangle_vertex(c1, r1, c2, r2)
    assert vertex[0] == pytest.approx(2.33, 0.01)  # x
    assert vertex[1] == pytest.approx(0, 0.01)  # y


def test_count_triangle_vertex_separated_randomly():
    """
    check if vertex of triangle is calculated correctly when circles are separated and their coordinates are not on
    x and y axes
    see picture visualizing the situation (./pictures/test_count_triangle_vertex_separated_randomly.jpg)
    """
    c1 = [2, 2]
    c2 = [6, 4]

    r1 = 2
    r2 = 1

    vertex = count_triangle_vertex(c1, r1, c2, r2)
    assert vertex[0] == pytest.approx(4.66, 0.01)  # x
    assert vertex[1] == pytest.approx(3.33, 0.01)  # y


def test_count_triangle_vertex_intersected_randomly():
    """
    check if vertex of triangle is calculated correctly when circles intersects and their coordinates are not on
    x and y axes
    see picture visualizing the situation (./pictures/test_count_triangle_vertex_intersected_randomly.jpg)
    """
    c1 = [2, 2]
    c2 = [7, 4]

    r1 = 2
    r2 = 4

    vertex = count_triangle_vertex(c1, r1, c2, r2)
    assert vertex[0] == pytest.approx(3.51, 0.01)  # x
    assert vertex[1] == pytest.approx(2.60, 0.01)  # y
