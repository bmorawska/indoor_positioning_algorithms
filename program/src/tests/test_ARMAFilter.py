import numpy as np

from src.UWB.filters.ARMA_filter import ARMAFilter


def test_median_filter():
    example_data = \
        np.array([
            (1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1),
            (1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1)
        ])

    tracker = ARMAFilter(alpha=0.8)

    prev = 1
    for i in range(len(example_data)):
        a, b = tracker.ARMA_step(example_data[i])
        assert a < 1
        assert a > 0
        assert a < 2 * prev

        prev = a
