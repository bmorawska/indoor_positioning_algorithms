import serial
import sys
import utm

# System settings
SERIAL_PORT_LINUX = '/dev/ttyUSB0'
BAUDRATE = 4800
TIMEOUT = 0

# Measure settings
NUMBER_OF_MEASURES = 2100
filename = str('reference.csv')

# Error code
INTERRUPTED_BY_USER = 0

file = open(filename, "w+")

# Must be set as a average of measures from ./single_GPS, when GPS does not move.
AVG_LONGITUDE = 5695187.57562368
AVG_LATITUDE = 375268.255968784

port = serial.Serial(SERIAL_PORT_LINUX, BAUDRATE)
counter = 0
while counter < NUMBER_OF_MEASURES:
    try:
        raw_data = port.readline().decode('ascii', errors='replace')
        data = raw_data.split(',')
        if data[0] == '$GPGGA':
            print(counter)
            latitude = float(data[2]) / 100
            if data[3] == b'S':
                latitude *= -1
            longitude = float(data[4]) / 100
            if data[5] == b'W':
                longitude *= -1

            cartesian_position = utm.from_latlon(latitude, longitude)
            time = data[1]
            fix_latitude = cartesian_position[0] - AVG_LATITUDE
            fix_longitude = cartesian_position[1] - AVG_LONGITUDE
            msg = "{},{},{},{},{}\n".format(data[1], cartesian_position[0], fix_latitude, cartesian_position[1],
                                            fix_longitude)
            print(msg)
            file.write(msg)
            counter += 1

    except (KeyboardInterrupt, SystemExit):
        file.close()
        print('\nProgram interrupted by user')
        sys.exit(INTERRUPTED_BY_USER)

file.close()
