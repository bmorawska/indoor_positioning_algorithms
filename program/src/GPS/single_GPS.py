import serial
import sys
import utm

# System settings
SERIAL_PORT_LINUX = '/dev/ttyUSB0'
BAUDRATE = 4800
TIMEOUT = 0

# Measure settings
NUMBER_OF_MEASURES = 3600
filename = str('singleGPS.csv')

# Error code
INTERRUPTED_BY_USER = 0

# Global variables
counter = 0

file = open(filename, "w+")
port = serial.Serial(SERIAL_PORT_LINUX, BAUDRATE)
while counter < NUMBER_OF_MEASURES:

    try:
        raw_data = port.readline().decode('ascii', errors='replace')
        data = raw_data.split(',')

        if data[0] == '$GPGGA':
            print(counter)
            latitude = float(data[2]) / 100
            if data[3] == b'S':
                latitude *= -1
            longitude = float(data[4]) / 100
            if data[5] == b'W':
                longitude *= -1

            cartesian_position = utm.from_latlon(latitude, longitude)
            file.write(data[1] + ',' + str(cartesian_position[0]) + ',' + str(cartesian_position[1]) + ',' + data[8]
                       + ',' + data[7] + '\n')
            counter += 1

    except (KeyboardInterrupt, SystemExit):
        file.close()
        print('\nProgram interrupted by user')
        sys.exit(INTERRUPTED_BY_USER)

file.close()
