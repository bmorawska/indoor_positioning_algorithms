import numpy as np


def is_inside_triangle(first_point, second_point, third_point, measured_point):
    s1 = third_point[1] - first_point[1]
    s2 = third_point[0] - first_point[0]
    s3 = second_point[1] - first_point[1]
    s4 = measured_point[1] - first_point[1]

    w1 = (first_point[0] * s1 + s4 * s2 - measured_point[0] * s1) / (s3 * s2 - (second_point[0] - first_point[0]) * s1)
    w2 = (s4 - w1 * s3) / s1

    return (w1 >= 0) and (w2 >= 0) and (w1 + w2) <= 1


def reduce_fake_point(first_point, second_point, third_point, points):
    if is_inside_triangle(first_point, second_point, third_point, points[0]):
        return points[0]
    elif is_inside_triangle(first_point, second_point, third_point, points[1]):
        return points[1]
    else:
        return None


def sqr(x):
    return x * x


def count_intersection_point(first_radius, second_radius, first_center, second_center):
    vector_length = np.linalg.norm(np.array(first_center) - np.array(second_center))
    cosinusA = (sqr(vector_length) + sqr(first_radius) - sqr(second_radius)) / (2 * vector_length * first_radius)
    sinusA = np.sqrt(1 - sqr(cosinusA))

    temp_vec = (np.subtract(second_center, first_center) / vector_length) * first_radius
    rotation_matrix = [(cosinusA, -sinusA), (sinusA, cosinusA)]
    first_point = np.dot(temp_vec, rotation_matrix) + first_center
    rotation_matrix = [(cosinusA, sinusA), (cosinusA, -sinusA)]
    second_point = np.dot(temp_vec, rotation_matrix) + first_center

    return np.array([first_point, second_point])


def get_triangle_points(first_anchor_position, second_anchor_position, third_anchor_position,
                        first_radius, second_radius, third_radius):

    first_intersections = count_intersection_point(first_radius,
                                                   second_radius,
                                                   first_anchor_position,
                                                   second_anchor_position)

    second_intersections = count_intersection_point(second_radius,
                                                    third_radius,
                                                    second_anchor_position,
                                                    third_radius)

    third_intersections = count_intersection_point(third_radius,
                                                   first_radius,
                                                   third_anchor_position,
                                                   first_anchor_position)

    first_triangle_point = reduce_fake_point(first_anchor_position,
                                             second_anchor_position,
                                             third_anchor_position,
                                             first_intersections)

    second_triangle_point = reduce_fake_point(first_anchor_position,
                                              second_anchor_position,
                                              third_anchor_position,
                                              second_intersections)

    third_triangle_point = reduce_fake_point(first_anchor_position,
                                             second_anchor_position,
                                             third_anchor_position,
                                             third_intersections)
    return np.array([first_triangle_point, second_triangle_point, third_triangle_point])


first = [1, 1]
second = [5, 5]
third = [0, 5]

r1 = 3
r2 = 4
r3 = 3

get_triangle_points(first, second, third, r1, r2, r3)
