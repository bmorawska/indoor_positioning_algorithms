import numpy as np


def get_triangle_points(first_anchor_position, second_anchor_position, third_anchor_position,
                        first_radius, second_radius, third_radius):

    v1 = np.array(second_anchor_position) - np.array(first_anchor_position)
    v2 = np.array(third_anchor_position) - np.array(second_anchor_position)
    v3 = np.array(first_anchor_position) - np.array(third_anchor_position)

    p1 = (first_radius / (second_radius + first_radius)) * v1 + first_anchor_position
    p2 = (second_radius / (second_radius + third_radius)) * v2 + second_anchor_position
    p3 = (third_radius / (third_radius + first_radius)) * v3 + third_anchor_position

    return np.array([p1, p2, p3])


def centre_of_gravity(triangle):
    center = (triangle[0] + triangle[1] + triangle[2]) / 3
    return center
