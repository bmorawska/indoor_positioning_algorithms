import serial
import sys
import datetime

# Settings and constants

SERIAL_PORT_LINUX = '/dev/ttyACM0'
BAUDRATE = 9600
TIMEOUT = 0

NUMBER_OF_MEASURES = 500
MILIMETERS_PER_METER = 1000
INTERRUPTED_BY_USER = 0

file_created = datetime.datetime.now()
counter = 0
filename = 'pomiar3.csv'
file = open(filename, "w+")

while counter < NUMBER_OF_MEASURES:

    try:
        port = serial.Serial(SERIAL_PORT_LINUX, BAUDRATE)
        raw_data = port.readline()
        data = raw_data.split()

        file.write(str(datetime.datetime.now().time()) + ',')

        if data[0] == b'mc':
            mask = int(data[1], 16)

            print(datetime.datetime.now().time())
            if mask & 0x01:
                range0 = int(data[2], 16) / MILIMETERS_PER_METER

                file.write(str(range0) + ',')
                print('Anchor 0: {} m'.format(range0))
            else:
                file.write('0' + ',')
                print('Anchor 0 not connected')

            if mask & 0x02:
                range1 = int(data[3], 16) / MILIMETERS_PER_METER
                file.write(str(range1) + ',')
                print('Anchor 1: {} m'.format(range1))
            else:
                file.write('0' + ',')
                print('Anchor 1 not connected')

            if mask & 0x04:
                range2 = int(data[4], 16) / MILIMETERS_PER_METER
                file.write(str(range2) + ',')
                print('Anchor 2: {} m'.format(range2))
            else:
                file.write('0' + ',')
                print('Anchor 2 not connected')

            if mask & 0x08:
                range3 = int(data[5], 16) / MILIMETERS_PER_METER
                file.write(str(range3) + ',')
                print('Anchor 3: {} m'.format(range3))
            else:
                file.write('0' + ',')
                print('Anchor 3 not connected')

    except (KeyboardInterrupt, SystemExit):
        file.close()
        print('\nProgram interrupted by user')
        sys.exit(INTERRUPTED_BY_USER)

    counter += 1
    print(counter)
    file.write('\n')

file.close()


