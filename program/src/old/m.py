from sympy import Circle, Triangle


def is_inside_triangle(triangle, point):
    s1 = triangle.vertices[2][1] - triangle.vertices[0][1]
    s2 = triangle.vertices[2][0] - triangle.vertices[0][0]
    s3 = triangle.vertices[2][1] - triangle.vertices[0][1]
    s4 = point[1] - triangle.vertices[0][1]

    w1 = (triangle.vertices[0][0] * s1 + s4 * s2 - point[0] * s1) / \
         (s3 * s2 - (triangle.vertices[1][0] - triangle.vertices[0][0]) * s1)
    w2 = (s4 - w1 * s3) / s1

    return (w1 >= 0) and (w2 >= 0) and (w1 + w2) <= 1


def get_point_inside_triangle(triangle, points):
    if len(points) == 0:
        return None
    else:
        if is_inside_triangle(triangle, points[0]):
            return points[0]
        elif is_inside_triangle(triangle, points[1]):
            return points[1]
        else:
            return None


def get_triangle_points(first_anchor_position, second_anchor_position, third_anchor_position,
                        first_radius, second_radius, third_radius):
    c1 = Circle(first_anchor_position, first_radius)
    c2 = Circle(second_anchor_position, second_radius)
    c3 = Circle(third_anchor_position, third_radius)
    t1 = Triangle(first_anchor_position, second_anchor_position, third_anchor_position)

    i1 = c1.intersection(c2)
    i2 = c2.intersection(c3)
    i3 = c3.intersection(c1)

    p1 = get_point_inside_triangle(t1, i1)
    p2 = get_point_inside_triangle(t1, i2)
    p3 = get_point_inside_triangle(t1, i3)

    if (p1 and p2 and p3) is not None:
        t2 = Triangle(p1, p2, p3)
        return t2
    else:
        return 1
        #raise ValueError('There is not three vertices in triangle.')


def get_triangle_area(triangle):
    return triangle.area.evalf()


def get_center_of_triangle(triangle):
    return triangle.centroid.evalf()
