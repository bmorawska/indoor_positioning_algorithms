import serial
import sys
import datetime

from src.UWB.geometry import count_vertices, count_center
from src.UWB.filters.kalman_filter import init_kalman_filter, kalman_step
from src.UWB.filters.median_filter import MedianFilter
from src.UWB.filters.ARMA_filter import ARMAFilter
from src.UWB.globals import NUMBER_OF_MEASURES, \
    INTERRUPTED_BY_USER, \
    FILENAME, \
    SERIAL_PORT_LINUX, \
    BAUDRATE, \
    MILLIMETERS_PER_METER, \
    FILTER

# File initialization
file = open(FILENAME, "w+")

if FILTER == FILTER.KALMAN:
    # Kalman filter initialization
    tracker = init_kalman_filter()
elif FILTER == FILTER.MEDIAN:
    # Median filter initialization
    tracker = MedianFilter()
elif FILTER == FILTER.ARMA:
    # Median filter initialization
    tracker = ARMAFilter()
else:
    tracker = None

counter = 0
while counter < NUMBER_OF_MEASURES:
    try:
        port = serial.Serial(SERIAL_PORT_LINUX, BAUDRATE)
        raw_data = port.readline()
        data = raw_data.split()
        file.write(str(datetime.datetime.now().time()) + ',')
        range0, range1, range2 = 0, 0, 0

        if data[0] == b'mc':
            mask = int(data[1], 16)

            print(datetime.datetime.now().time())
            if mask & 0x01:
                range0 = int(data[2], 16) / MILLIMETERS_PER_METER
                file.write(str(range0) + ',')
                print('Anchor 0: {} m'.format(range0))
            else:
                file.write('0' + ',')
                print('Anchor 0 not connected')

            if mask & 0x02:
                range1 = int(data[3], 16) / MILLIMETERS_PER_METER
                file.write(str(range1) + ',')
                print('Anchor 1: {} m'.format(range1))
            else:
                file.write('0' + ',')
                print('Anchor 1 not connected')

            if mask & 0x04:
                range2 = int(data[4], 16) / MILLIMETERS_PER_METER
                file.write(str(range2) + ',')
                print('Anchor 2: {} m'.format(range2))
            else:
                file.write('0' + ',')
                print('Anchor 2 not connected')

    except (KeyboardInterrupt, SystemExit):
        file.close()
        print('\nProgram interrupted by user')
        sys.exit(INTERRUPTED_BY_USER)

    if (range0 and range1 and range2) != 0:
        triangle = count_vertices(range0, range1, range2)
        center = count_center(triangle)
        print("center: ", center)
        file.write(str(center[0]) + "," + str(center[1]) + ",")

        if FILTER == FILTER.KALMAN:
            kalman_step(tracker, measurement=center)
            print("kalman center: {}, {}; velocity: {}, {}".format(tracker.x[0], tracker.x[2], tracker.x[1],
                                                                   tracker.x[3]))
            file.write(str(tracker.x[0]) + ',' + str(tracker.x[2]) + ',' + str(tracker.x[1]) + ',' + str(tracker.x[3]))
        elif FILTER == FILTER.MEDIAN:
            x, y = tracker.median_step(measurement=center)
            print("median center: {}, {}".format(x, y))
            file.write(str(x) + "," + str(y))
        elif FILTER == FILTER.ARMA:
            x, y = tracker.ARMA_step(measurement=center)
            print("ARMA center: {}, {}".format(x, y))
            file.write(str(x) + "," + str(y))

    counter += 1
    print(counter)
    file.write('\n')

file.close()
