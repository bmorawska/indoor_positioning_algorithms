import numpy as np

from src.UWB.globals import ANCH_0, ANCH_1, ANCH_2


def count_center(points):
    centroid = (points[0] + points[1] + points[2]) / 3
    return centroid


def count_triangle_vertex(first_circle_position, first_radius, second_circle_position, second_radius):

    vec_center_center = np.array(first_circle_position) - np.array(second_circle_position)
    norm = np.linalg.norm(vec_center_center)
    d = norm - first_radius - second_radius

    if (first_radius + second_radius) < norm:
        u2 = d / (first_radius / second_radius + 1)
        vec = vec_center_center * ((u2 + second_radius) / norm) + second_circle_position
    else:
        d *= -1
        u2 = d / (first_radius / second_radius + 1)
        u1 = d - u2
        vec = vec_center_center * ((second_radius - u1) / norm) + second_circle_position
    return vec


def count_vertices(range1, range2, range3):
    v1 = count_triangle_vertex(ANCH_0, range1, ANCH_1, range2)
    v2 = count_triangle_vertex(ANCH_1, range2, ANCH_2, range3)
    v3 = count_triangle_vertex(ANCH_2, range3, ANCH_0, range1)

    return np.array([v1, v2, v3])
