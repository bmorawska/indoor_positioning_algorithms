from numpy import array, tile, dot
from numpy.linalg import norm
from filterpy.kalman import KalmanFilter

from src.UWB.variance import mean_variance_on_axes
from src.UWB.globals import sqr

"""
Filter parameters at the beginning
"""
T = 0.1  # interval between measurements in seconds

Fi = array([
    [1, T, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, T],
    [0, 0, 0, 1]
])

H = array([
    [1, 0, 0, 0],
    [0, 0, 1, 0]
])

x = array([
    [0, 0, 0, 0]
]).T

P = array([
    [10,  0,  0,  0],
    [ 0, 10,  0,  0],
    [ 0,  0, 10,  0],
    [ 0,  0,  0, 10]
])

Q = array([
    [sqr(sqr(T)) / 4, T * sqr(T) / 2,               0,              0],
    [ T * sqr(T) / 3,         sqr(T),               0,              0],
    [              0,              0, sqr(sqr(T)) / 4, T * sqr(T) / 2],
    [              0,              0,  T * sqr(T) / 2,         sqr(T)]
])

point = array([3, 3])

"""
Filter implementation
"""


def init_kalman_filter():
    tracker = KalmanFilter(dim_x=4, dim_z=2)
    tracker.F = Fi
    tracker.H = H
    tracker.R = array([
        [1, 0],
        [0, 1]
    ])
    tracker.x = x
    tracker.P = P
    tracker.u = 0.
    tracker.Q = Q

    return tracker


def kalman_step(tracker, measurement):
    z = array(measurement).T
    tracker.predict(tracker.Q)
    tracker.update(z)

    sigma = mean_variance_on_axes(point)
    sigma_x = norm(sigma[0])
    sigma_y = norm(sigma[1])
    R = array([
        [sqr(sigma_x), sigma_x * sigma_y],
        [sigma_x * sigma_y, sqr(sigma_y)]
    ])

    R = tile(R, (2, 2))
    tracker.Q = dot(tracker.Q, R)

    return tracker.x
