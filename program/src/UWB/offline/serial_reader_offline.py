import csv

from src.UWB.filters.ARMA_filter import ARMAFilter
from src.UWB.filters.median_filter import MedianFilter
from src.UWB.filters.kalman_filter import init_kalman_filter, kalman_step

# Settings
filename = '../../offline2.csv'
window_size = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
alpha = [0.5, 0.75, 0.8, 0.9, 0.95]

file = None

for i in range(len(window_size)):
    result_file = filename[0: len(filename) - 4] + "_results_window_size_" + str(window_size[i]) + ".csv"
    file = open(result_file, "w+")

    tracker_median = MedianFilter(window_size=window_size[i])
    with open(filename, mode='r') as csv_file:
        readCSV = csv.reader(csv_file, delimiter=',')
        for row in readCSV:
            if row[4] and row[5]:
                center = [float(row[4]), float(row[5])]
                file.write(str(center[0]) + "," + str(center[1]) + ",")
                x, y = tracker_median.median_step(measurement=center)
                file.write(str(x) + "," + str(y) + ",\n")

    print("Mediana okno: ", str(window_size[i]), "ukończone")

file.close()
print("Mediana skończona")

for i in range(len(alpha)):
    index = str(alpha[i] * 100)[0] + str(alpha[i] * 100)[1]
    result_file = filename[0: len(filename) - 4] + "_results_alpha_" + str(index) + ".csv"
    file = open(result_file, "w+")

    tracker_arma = ARMAFilter(alpha=alpha[i])
    with open(filename, mode='r') as csv_file:
        readCSV = csv.reader(csv_file, delimiter=',')
        for row in readCSV:
            if row[4] and row[5]:
                center = [float(row[4]), float(row[5])]
                file.write(str(center[0]) + "," + str(center[1]) + ",")
                x, y = tracker_arma.ARMA_step(measurement=center)
                file.write(str(x) + "," + str(y) + ",\n")

    print("ARMA alfa: ", str(alpha[i]), "ukończone")

file.close()
print("ARMA skończone")

tracker_kalman = init_kalman_filter()
result_file = filename[0: len(filename) - 4] + "_results_kalman.csv"
file = open(result_file, "w+")
with open(filename, mode='r') as csv_file:
    readCSV = csv.reader(csv_file, delimiter=',')
    for row in readCSV:
        if row[4] and row[5]:
            center = [float(row[4]), float(row[5])]
            file.write(str(center[0]) + "," + str(center[1]) + ",")
            kalman_step(tracker_kalman, measurement=center)
            msg = str(tracker_kalman.x[0][0]) + ',' + str(tracker_kalman.x[2][0]) + ',' + \
                  str(tracker_kalman.x[1][0]) + ',' + str(tracker_kalman.x[3][0]) + ",\n"
            file.write(msg)

print("Kalman skończony")

file.close()
