# Badanie charakterystyki błędu pomiaru z urządzenia TREK1000

## Środowisko

**miejsce**: mój pokój

**data**: 21 lipca 2019

**wykorzystane urządzenia**: 
- 3 kotwice (1: ON, 2: ON, 3: ON, 4: ON)
- 1 tag (1: ON, 2: ON, 3: ON, 4: OFF)
- mój laptop

**cel**: sprawdzenie jak wygląda charakterystyka błędu pomiaru urządzenia


## Test 1 - 3 kotwice w odległości 30cm od taga

**Liczba pomaiarów**: 1000

**Plik wynikowy**:  30cm_3_kotwice_trojkat.csv

**Skrypt w matlabie**:
```
qqq=load('30cm_3_kotwice_trojkat.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
%histogram(range0, 10)
histogram(range1, 10)
histogram(range2, 10)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')
```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_1/Test_1_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie z widocznymi 2 kotwicami](./obrazy/Test_1/test_1_rozklad_widac_2_kotwice.png "Rozkład błędu na histogramie z widocznymi 2 kotwicami")
Rozkład błędu na histogramie z widocznymi 2 kotwicami

![Rozkład błędu na histogramie z widocznymi 3 kotwicami](./obrazy/Test_1/test_1_rozklad_widac_3_kotwice.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami")
Rozkład błędu na histogramie z widocznymi 3 kotwicami

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_1/test_1_range0_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0

![Rozkład błędu w czasie dla kotwicy o ID = 1](./obrazy/Test_1/test_1_range1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 1")
Rozkład błędu w czasie dla kotwicy o ID = 1


![Rozkład błędu w czasie dla kotwicy o ID = 2](./obrazy/Test_1/test_1_range2_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 2")
Rozkład błędu w czasie dla kotwicy o ID = 2

Dla bardzo małych odległości (tutaj 30cm) nie udało się ustalić charakterystyki błędu. Zachowanie kotwicy 0 mocno odbiega od zachowania kotwic 1 i 2. Możliwe, że jest to wina zakłóceń.

## Test 2 - 3 kotwice w odległości 1m od taga

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  1m_3_kotwice_trojkat.csv

**Skrypt w matlabie**:
```
qqq=load('1m_3_kotwice_trojkat.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
histogram(range1, 50)
histogram(range2, 50)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')

```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_2/test_2_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie z widocznymi 3 kotwicami](./obrazy/Test_2/test_2_rozklad.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami")
Rozkład błędu na histogramie z widocznymi 3 kotwicami

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_2/test_2_range0_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0

![Rozkład błędu w czasie dla kotwicy o ID = 1](./obrazy/Test_2/test_2_range1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 1")
Rozkład błędu w czasie dla kotwicy o ID = 1


![Rozkład błędu w czasie dla kotwicy o ID = 2](./obrazy/Test_2/test_2_range2_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 2")
Rozkład błędu w czasie dla kotwicy o ID = 2

Przy większej odległości oraz precyzyjniej zmierzonej odległości można zauważyć charakterystykę podobną do Gaussa lub Poissona. Ciekawym zjawiskiem jest to, że mimo, że kotwice były ustawione od taga o równo 1m to odległość obliczona przez urządzenie wynosi około 10cm. Słabe wyniki kotwicy 2 mogą być spowodowane tym, że kotwica 0 i 1 miała antenę położoną równolegle do taga, a kotwica 2 - prostopadle.

srednia =

    0.8898    0.9193    0.9168


mediana =

    0.8910    0.9190    0.9140


dominanta =

    0.8910    0.9190    0.9000


wariancja =

   1.0e-03 *

    0.6822    0.5143    0.4018


srednie_odchylenie =

    0.0261    0.0227    0.0200

## Test 3  - 3 kotwice obok siebie w odległości 2m (z Koresią)

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  2m_3_kotwice_obok_siebie.csv

**Skrypt w matlabie**:
```
qqq=load('2m_3_kotwice_obok_siebie.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
histogram(range1, 50)
histogram(range2, 50)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')
```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_3/test_3_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie z widocznymi 3 kotwicami, pelny z widocznym zakłóceniem (Koresia wlazła)](./obrazy/Test_3/test_3_rozklad_pelny.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami, pelny z widocznym zakłóceniem (Koresia wlazła)")
Rozkład błędu na histogramie z widocznymi 3 kotwicami, pelny z widocznym zakłóceniem (Koresia wlazła)

![Rozkład błędu na histogramie z widocznymi 3 kotwicami, obciety bez widocznych zakłóceń](./obrazy/Test_3/test_3_rozklad_obciety.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami, obciety bez widocznych zakłóceń")
Rozkład błędu na histogramie z widocznymi 3 kotwicami, obciety bez widocznych zakłóceń

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_3/test_3_range0_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0

![Rozkład błędu w czasie dla kotwicy o ID = 1](./obrazy/Test_3/test_3_range1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 1")
Rozkład błędu w czasie dla kotwicy o ID = 1

**Tu widać Koresię i mnie.**


![Rozkład błędu w czasie dla kotwicy o ID = 2](./obrazy/Test_3/test_3_range2_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 2")
Rozkład błędu w czasie dla kotwicy o ID = 2

**A tu tylko moją nogę.**

Przy większej odległości oraz precyzyjniej zmierzonej odległości można zauważyć charakterystykę podobną do Gaussa lub Poissona. Przypadkowym, ale ciekawym zjawiskiem jest zakłócenie spowodowane przejściem moim i Koresi przez środek układu. Zakłóciłyśmy przebieg fali przez co odległość zmieniła się na krótki moment o 0.5m. Zaskakujący jest dwumodalny przebieg kotwicy na środku. Możliwe, że jest on wywołany przez obecność dwóch pozostałych anten po bokach.

srednia =

    1.9857    1.9885    1.9559


mediana =

    1.9860    1.9810    1.9580


dominanta =

    1.9480    1.9620    1.9620


wariancja =

    0.0017    0.0044    0.0006


srednie_odchylenie =

    0.0410    0.0662    0.0249


## Test 4 - 3 kotwice obok siebie w odległości 2m (bez Koresi)

**Jest to powtórzone doświadczenie 3 tylko nic nie zakłóciło przebiegu**

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  2m_3_kotwice_obok_siebie_ujecie_2.csv

**Skrypt w matlabie**:
```
qqq=load('2m_3_kotwice_obok_siebie_ujecie_2.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
histogram(range1, 50)
histogram(range2, 50)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')

```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_4/test_4_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie z widocznymi 3 kotwicami](./obrazy/Test_4/test_4_rozklad.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami")
Rozkład błędu na histogramie z widocznymi 3 kotwicami

![Rozkład błędu na histogramie z widoczną kotwicą 0.](./obrazy/Test_4/test_4_rozklad_tylko_anchor0.png "Rozkład błędu na histogramie z widoczną kotwicą 0.")
Rozkład błędu na histogramie z widoczną kotwicą 0.

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_4/test_4_range0_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0

![Rozkład błędu w czasie dla kotwicy o ID = 1](./obrazy/Test_4/test_4_range1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 1")
Rozkład błędu w czasie dla kotwicy o ID = 1

![Rozkład błędu w czasie dla kotwicy o ID = 2](./obrazy/Test_4/test_4_range2_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 2")
Rozkład błędu w czasie dla kotwicy o ID = 2

Mimo braku zakłócenia nadal widać, że kotwica numer 0 ma charakter dwumodalny. Co ciekawe, jako jedyna ma "środek"w 2m, czyli w odległości, w jakiej naprawdę zanjduje się kotwica. Na rozkładach błędu w czasie widać, że powstają odchylenia i że występują one razem w zbiorowiskach.


srednia =

    1.9905    1.9485    1.9536


mediana =

    1.9910    1.9480    1.9530


dominanta =

    2.0280    1.9620    1.9620


wariancja =

    0.0014    0.0007    0.0004


srednie_odchylenie =

    0.0373    0.0263    0.0202
 
 ## Test 5 - 1 kotwica w odległości 2m od taga

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  2m_3_kotwice_obok_siebie_ujecie_3.csv

**Skrypt w matlabie**:
```
qqq=load('2m_3_kotwice_obok_siebie_ujecie_3.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
%histogram(range1, 50)
%histogram(range2, 50)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')

```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_5/test_5_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie z widocznymi 3 kotwicami](./obrazy/Test_5/test_5_rozklad.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami")
Rozkład błędu na histogramie z widocznymi 3 kotwicami

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_5/test_5_range0_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0


Zachodziło podejrzenie, że anteny położone zbyt blisko siebie mogą się wzajemnie zakłócać. Dlatego podjęta została próba sprawdzenia jak zachowa się pojedyncza kotwica. Wypadła wzorowo. Rozkład Gaussa, z odchyleniem w 2m, czyli najczęściej obliczaną wartością była ta prawidłowa.



srednia =

    2.0025  


mediana =

    2.0050 

dominanta =

    2.0190 


wariancja =

    0.0013


srednie_odchylenie =

    0.0355

## Test 6 - 3 kotwice obok siebie w odległości 4m

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  4m_3_kotwice_obok_siebie.csv

**Skrypt w matlabie**:
```
qqq=load('4m_3_kotwice_obok_siebie.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
histogram(range1, 50)
histogram(range2, 50)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')

```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_6/test_6_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie z widocznymi 3 kotwicami](./obrazy/Test_6/test_6_rozklad.png "Rozkład błędu na histogramie z widocznymi 3 kotwicami")
Rozkład błędu na histogramie z widocznymi 3 kotwicami

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_6/test_6_range0_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0

![Rozkład błędu w czasie dla kotwicy o ID = 1](./obrazy/Test_6/test_6_range1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 1")
Rozkład błędu w czasie dla kotwicy o ID = 1

![Rozkład błędu w czasie dla kotwicy o ID = 2](./obrazy/Test_6/test_6_range2_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 2")
Rozkład błędu w czasie dla kotwicy o ID = 2

Wyniki na tą odległość wydają się być idealne i zgodne z oczekiwaniami. Rozkład przypomina w każdym przypadku ``zgrubsza`` Gaussowski, a odległości odpowiadają rzeczywistym.

srednia =

    3.9913    4.0516    3.9589


mediana =

    3.9890    4.0550    3.9560


dominanta =

    3.9940    4.0590    3.9560


wariancja =

    0.0012    0.0008    0.0006


srednie_odchylenie =

    0.0352    0.0276    0.0249
