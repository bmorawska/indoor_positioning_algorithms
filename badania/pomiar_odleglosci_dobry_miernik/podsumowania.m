prawdziwa_odleglosc = [0.854...
1.459...
1.603...
2.189...
2.903...
2.954...
4.112...
6.488...
6.762...
6.98...
8.943...
11.492...
13.039...
16.501...
23.673...
];

srednia = [0.8455...
1.533...
1.5661...
2.1063...
3.0055...
2.8678...
4.0322...
6.6098...
6.6734...
6.936...
8.8916...
11.5742...
12.9155...
16.581...
23.7337...
];

mediana = [0.849...
1.536...
1.569...
2.107...
3.006...
2.87...
4.036...
6.608...
6.674...
6.936...
8.895...
11.575...
12.916...
16.582...
23.737...
];

dominanta = [0.849...
1.555...
1.574...
2.07...
3.001...
2.879...
4.041...
6.608...
6.683...
6.931...
8.9...
11.58...
12.921...
16.572...
23.746...
];

wariancja = [0.0019...
0.009494...
0.0019...
0.0007...
0.006316...
0.0004...
0.0008...
0.00741...
0.0005342...
0.0005...
0.0005735...
0.003866...
0.004648...
0.006245...
0.007478...
];

srednie_odchylenie = [0.0435...
0.0308...
0.044...
0.0262...
0.0251...
0.0195...
0.0276...
0.0272...
0.0231...
0.0215...
0.0239...
0.0197...
0.0216...
0.025...
0.0273...
];

roznica_srednia = [-0.0085...
0.074...
-0.0369...
-0.0827...
0.1025...
-0.0862...
-0.0798...
0.1218...
-0.0886...
-0.044...
-0.0514...
0.0822...
-0.1235...
0.08...
0.0607...
];

roznica_dominanta = [-0.005...
0.096...
-0.029...
-0.119...
0.098...
-0.075...
-0.071...
0.12...
-0.079...
-0.049...
-0.043...
0.088...
-0.118...
0.071...
0.073...
];

roznica_mediana = [-0.005...
0.077...
-0.034...
-0.082...
0.103...
-0.084...
-0.076...
0.12...
-0.088...
-0.044...
-0.048...
0.083...
-0.123...
0.081...
0.064...
];


plot(prawdziwa_odleglosc, roznica_srednia,...
    prawdziwa_odleglosc, roznica_mediana,...
    prawdziwa_odleglosc, roznica_dominanta)

plot(prawdziwa_odleglosc, ro)

legend('dominanta', 'medoana', 'srednia' )
