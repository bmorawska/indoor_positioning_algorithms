A0=autocorr(range0);
A1=autocorr(range1);
A2=autocorr(range2);

figure(3)
hold on
h0=stem(A0);
h1=stem(A1);
h2=stem(A2);
hold off
set(h0, 'Color', [0, 0, 1],'MarkerFaceColor', [0, 0, 1]);
set(h1, 'Color', [0, 0.5, 0],'MarkerFaceColor', [0, 0.5, 0]);
set(h2, 'Color', [1, 0, 0],'MarkerFaceColor', [1, 0, 0]);
set (gca, 'XLim', [1 20], 'YLim', [-0.03 +0.03])

legend('kotwica 1', 'kotwica 2', 'kotwica 3')
