A0=crosscorr(range0, range1);
A1=crosscorr(range1, range2);
A2=crosscorr(range2, range0);


hold on
subplot(3,1,1)
h0=stem(A0);
subplot(3,1,2)
h1=stem(A1);
subplot(3,1,3)
h2=stem(A2);
hold off

set(h0, 'Color', [0, 0, 1],'MarkerFaceColor', [0, 0, 1]);
set(h1, 'Color', [0, 0.5, 0],'MarkerFaceColor', [0, 0.5, 0]);
set(h2, 'Color', [1, 0, 0],'MarkerFaceColor', [1, 0, 0]);
set (gca, 'XLim', [1 20], 'YLim', [-0.03 +0.03])



