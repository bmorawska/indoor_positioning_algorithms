dane = load('singleGPS.csv', '-ascii');
czas = dane(:, 1);
dlugosc = dane(:,2);
szerokosc = dane(:,3);
rozmycie = dane(:,4);
satelity = dane(:,7);

hold on;
plot(czas, rozmycie)
plot(czas, satelity, 'r.')
ylim([0.9 1.6])
hold off

