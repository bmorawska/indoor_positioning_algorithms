function y=filtruj_zera(x)

y=[];
for i=1:length(x)
    if x(i) > 0.05
        y = [y x(i)];
    end
end
