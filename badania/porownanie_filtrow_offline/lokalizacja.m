%alpha 50
%alpha 75
%alpha 80
%alpha 95

%windows size 3
%windows size 5
%windows size 7
%windows size 9
%windows size 11
%windows size 13
%windows size 15
%windows size 17
%windows size 19
%windows size 21

titles = {...
    './results/offline1_results_kalman.csv',...
    './results/offline1_results_alpha_50.csv',...
    './results/offline1_results_alpha_75.csv',...
    './results/offline1_results_alpha_80.csv',...
    './results/offline1_results_alpha_90.csv',...
    './results/offline1_results_alpha_95.csv',...
    './results/offline1_results_window_size_3.csv',...
    './results/offline1_results_window_size_5.csv',...
    './results/offline1_results_window_size_7.csv',...
    './results/offline1_results_window_size_9.csv',...
    './results/offline1_results_window_size_11.csv',...
    './results/offline1_results_window_size_13.csv',...
    './results/offline1_results_window_size_15.csv',...
    './results/offline1_results_window_size_17.csv',...
    './results/offline1_results_window_size_19.csv',...
    './results/offline1_results_window_size_21.csv'...
};

for c = 1:length(titles)  
    tit = titles{c}
    c
    f=load(tit, '-ascii');
    fx=f(:,3);
    fy=f(:,4);
    MX = mean(fx);
    MY = mean(fy);
    fx = fx - MX;
    fy = fy - MY;
    fx = fx * 100;
    fy = fy * 100;

    figure(c)
    densityplot(fx,fy, {-4:0.3:4 -4:0.3:4});
    xlabel('b??d po?o?enia na osi x [cm]')
    ylabel('b??d po?o?enia na osi y [cm]')
    xlim([-4 4]);
    ylim([-4 4]);
end





