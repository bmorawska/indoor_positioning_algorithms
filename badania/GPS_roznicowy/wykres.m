dane=load('reference.csv', '-ascii');
czas=dane(:,1);
pomiar_szerokosci_referencyjny=dane(:,2);
poprawka_na_szerokosc=dane(:,3);
pomiar_dlugosci_referencyjny=dane(:,4);
poprawka_na_dlugosc=dane(:,5);
pomiar_szerkosci_zmierzony=dane(:,6);
pomiar_dlugosci_zmierzony=dane(:,7);
poprawiona_szerokosc=dane(:,8);
poprawiona_dlugosc=dane(:,9);

mean_szerokosc = mean(pomiar_szerkosci_zmierzony)
mean_dlugosc = mean(pomiar_dlugosci_zmierzony)
pomiar_dlugosci_zmierzony = pomiar_dlugosci_zmierzony - mean_dlugosc;
pomiar_szerkosci_zmierzony = pomiar_szerkosci_zmierzony - mean_szerokosc;
poprawiona_dlugosc = poprawiona_dlugosc - mean_dlugosc;
poprawiona_szerokosc = poprawiona_szerokosc - mean_szerokosc;

grid on
hold on
plot(pomiar_dlugosci_zmierzony, pomiar_szerkosci_zmierzony, 'b.')
plot(poprawiona_dlugosc,poprawiona_szerokosc, 'r.')
hold off