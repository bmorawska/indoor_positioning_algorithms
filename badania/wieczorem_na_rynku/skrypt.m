
qqq=load('pomiar3.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));

range1=range1(10:length(range1));
range2=range2(10:length(range2));
range0=range0(10:length(range0));
hold on
%histogram(range0, 50)
histogram(range1, 50)
%histogram(range2, 50)
hold off

legend('Anchor 0', 'Anchor 1', 'Anchor 2')
%xlim([1.7 2.2]);
title('Rozklad bledu pomiaru urzadzenia przy nieruchomym tagu')
xlabel('wystapienia')
ylabel('wartosc pomiaru')
%?rednie
srednia = [mean(range0), mean(range1),mean(range2)]
mediana = [median(range0), median(range1),median(range2)]
dominanta = [mode(range0), mode(range1),mode(range2)]
wariancja = [var(range0), var(range1), var(range2)]
srednie_odchylenie = [std(range0), std(range1), std(range2)]
