# Badanie błędu pomiaru TREK1000 w zależności od odległości taka od kotwicy

## Środowisko

**miejsce**: podwórko

**data**: 22 lipca 2019

**wykorzystane urządzenia**: 
- 3 kotwice (1: ON, 2: ON, 3: ON, 4: ON) (w ostatnim teście 2: OFF)
- 1 tag (1: ON, 2: ON, 3: ON, 4: OFF)  (w ostatnim teście 2: OFF)
- mój laptop

**cel**: jak wygląda błąd w zależności od odległości urządzeń.


## Test 1 Pomiar dla 2m 20cm odległości

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  2m20cm_1_kotwica.csv

**Skrypt w matlabie**:
```
qqq=load('2m20cm_1_kotwica.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
%histogram(range1, 50)
%histogram(range2, 50)
hold off

```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_1/test_1_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie dla jednej kotwicy i jednago taga](./obrazy/Test_1/test1_rozklad.png "Rozkład błędu na histogramie dla jednej kotwicy i jednago taga")
Rozkład błędu na histogramie dla jednej kotwicy i jednago taga

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_1/test1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0


srednia =

    2.1589  


mediana =

    2.1590 

dominanta =

    2.1640 


wariancja =

   1.0e-03 *

    0.4811 


srednie_odchylenie =

    0.0219 

## Test 2 Pomiar dla 4m 35cm odległości

**Liczba pomaiarów**: 10000

**Plik wynikowy**:  4m35cm_1_kotwica.csv

**Skrypt w matlabie**:
```
qqq=load('4m35cm_1_kotwica.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
histogram(range0, 50)
%histogram(range1, 50)
%histogram(range2, 50)
hold off

```


**Układ kotwic**

![Schemat układu urządzeń podczas przeprowadzania eksperymentu](./obrazy/Test_2/test_1_schemat.png "Schemat układu urządzeń podczas przeprowadzania eksperymentu")
Schemat układu urządzeń podczas przeprowadzania eksperymentu

### Wyniki badań

![Rozkład błędu na histogramie dla jednej kotwicy i jednago taga](./obrazy/Test_1/test1_rozklad.png "Rozkład błędu na histogramie dla jednej kotwicy i jednago taga")
Rozkład błędu na histogramie dla jednej kotwicy i jednago taga

![Rozkład błędu w czasie dla kotwicy o ID = 0](./obrazy/Test_1/test1_blad_w_czasie.png "Rozkład błędu w czasie dla kotwicy o ID = 0")
Rozkład błędu w czasie dla kotwicy o ID = 0


srednia =

    2.1589  


mediana =

    2.1590 

dominanta =

    2.1640 


wariancja =

   1.0e-03 *

    0.4811 


srednie_odchylenie =

    0.0219 
