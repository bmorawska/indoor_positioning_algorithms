qqq=load('1m_6i8mbps.csv', '-ascii');
range0=filtruj_zera(qqq(:,2));
range1=filtruj_zera(qqq(:,3));
range2=filtruj_zera(qqq(:,4));
hold on
grid on
histfit(range0, 34, 'lognormal')
%histogram(range0, 50)
%histogram(range1, 50)
%histogram(range2, 50)
hold off

%legend('Anchor 0', 'Anchor 1', 'Anchor 2')
%xlim([1.7 2.2]);
%title('Rozklad bledu pomiaru urzadzenia przy nieruchomym tagu')
xlabel('wystapienia')
ylabel('wartosc pomiaru odleg?o?ci')
%?rednie
srednia = [mean(range0), mean(range1),mean(range2)]
mediana = [median(range0), median(range1),median(range2)]
dominanta = [mode(range0), mode(range1),mode(range2)]
wariancja = [var(range0), var(range1), var(range2)]
srednie_odchylenie = [std(range0), std(range1), std(range2)]
