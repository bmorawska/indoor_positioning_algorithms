dane = load('zebrane_wartosci.csv', '-ascii');
realne_odleglosci = dane(:, 1);
srednia_arytmetyczna = dane(:,2);
mediana = dane(:,3);
dominanta = dane(:,4);

wariancja = dane(:, 5);

roznice_srednia = abs(realne_odleglosci - srednia_arytmetyczna);
roznice_mediana = abs(realne_odleglosci - mediana);
roznice_dominanta = abs(realne_odleglosci - dominanta);

hold on;
plot(realne_odleglosci, roznice_srednia)
plot(realne_odleglosci, roznice_mediana)
plot(realne_odleglosci, roznice_dominanta)
hold off

legend('srednia', 'mediana', 'dominanta')

%plot(realne_odleglosci, wariancja);
