# Algorytmy optymalizacjne wyznaczania pozycji wewnątrz budynków


## Wstęp
Repozytorium to dotyczy pracy inżynierskiej na temat algorytmów optymalizacji wyznaczania pozycji wewnątrz budynków oraz filtrowania otrzymanych wyników tak, żeby uzyskać jak najmniejszy błąd pomiaru. 

Praca realizowana na Politechnice Łódzkiej w roku 2019 na wydziale Fizyki Technicznej, Informatyki i Matematyki Stosowanej w Instytucie Informatyki.

## Wymagania do odczytu za pomocą portu szeregowego
- **Linux** - Należy upewnić się, że użytkownik ma dostęp do portu szergowego urządzenia. Aby jednorazowo przyznać pozwolenie należy uruchomić komendę:
```
sudo chmod 666 /dev/ttyACM0
```



## Podgląd danych z TREK1000

Na linuksie dane można obejrzeć przypinając **kotwicę** do komputera i wpisując w okno terminala komendę:

```
$: /dev/usb$ minicom -D /dev/ttyACM0
```

Oczekiwany wynik to:

```
mc 01 000011a7 00000000 00000000 00000000 1959 40 000a03b0 a0:0
mr 01 0000112f 00000000 00000000 00000000 1959 40 40354035 a0:0
mc 01 0000119e 00000000 00000000 00000000 195a 41 000a0414 a0:0
mr 01 00001126 00000000 00000000 00000000 195a 41 40354035 a0:0
mc 01 00001199 00000000 00000000 00000000 195b 42 000a0477 a0:0

```

Do wykonania polecenia niezbędny jest program ``minicom``
```
 sudo apt-get install minicom
```

